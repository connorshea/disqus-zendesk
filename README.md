# Disqus for Zendesk

A basic integration with Zendesk for working-with and responding-to Disqus comments.

## Resources

- [Zendesk Developer Portal](https://developer.zendesk.com/)
- [Zendesk Channel Framework Docs](https://developer.zendesk.com/apps/docs/channels-framework/introduction)
- [Zendesk Channel Framework Help Center Articles](https://help.zendesk.com/hc/en-us/sections/206223828-Channel-framework)
- [Zendesk Channel Framework API Docs](https://developer.zendesk.com/rest_api/docs/core/channel_framework)
- [Disqus API](https://disqus.com/api/docs/)

## Architecture

This "Zendesk Channel" will be built using Ruby. It'll need to be hosted by us in some fashion (presumably on Heroku so the DevOps team doesn't need to worry about it as much).

Some problems I see right-off-the-bat:

- Zendesk doesn't seem to have many great guides or examples of custom channels.
- The framework is relatively new (September 2016)
- I'm unsure whether we'd be able to respond using personal accounts for each agent, we may need to have anyone responding via Zendesk use a generic GitLab Disqus account.
